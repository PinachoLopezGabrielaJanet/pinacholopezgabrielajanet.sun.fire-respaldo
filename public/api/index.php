<?php
require '../../app/vendor/autoload.php';
$app = new \Slim\Slim();
$app->config(array(
    'templates.path' => '../../app/templates/'
));

//require '../../app/routes/root.php';
require '../../app/routes/xml.php';
require '../../app/routes/tema1.php';
require '../../app/routes/tema2.php';
require '../../app/routes/tema3.php';
require '../../app/routes/entidades.php';
require '../../app/routes/municipio.php';
//require '../../app/routes/xml.php';
$app->run();
